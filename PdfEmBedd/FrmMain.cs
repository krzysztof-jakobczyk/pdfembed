﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace PdfEmBed
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lbAttachments.SelectedItems.Count > 0)
                for (int i = 0; i < lbAttachments.SelectedItems.Count; i++)
                    lbAttachments.Items.Remove(lbAttachments.SelectedItems[i]);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (ofdInputfile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                tbInputFile.Text = ofdInputfile.FileName;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (ofdAttachment.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                foreach (string file in ofdAttachment.FileNames)
                    lbAttachments.Items.Add(file);
        }

        private void btnEmbedd_Click(object sender, EventArgs e)
        {
            try{
                string outputFileName = string.Format("{0}\\{1}_embedded.pdf",
                    Path.GetDirectoryName(tbInputFile.Text),
                    Path.GetFileNameWithoutExtension(tbInputFile.Text));

                PdfReader prInputFile = new PdfReader(tbInputFile.Text);
                PdfStamper psOutputFile = new PdfStamper(prInputFile, new FileStream(outputFileName,
                    FileMode.Create));

                foreach (string attachment in lbAttachments.Items)
                {
                    string fileName = Path.GetFileName(attachment);

                    PdfFileSpecification fs = PdfFileSpecification.FileEmbedded(psOutputFile.Writer,
                        attachment,
                        fileName,
                        null);

                    psOutputFile.AddFileAttachment(fileName, fs);
                }

                psOutputFile.Close();

                MessageBox.Show(string.Format("The files has been embedded. The output file is located in {0}.", outputFileName), "Process finished", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            catch(System.IO.IOException ioe){
                MessageBox.Show(string.Format("The process failed with following error: {0}", ioe.Message), "Process failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            FrmAbout frmAbout = new FrmAbout();
            frmAbout.ShowDialog();
        }
    }
}
