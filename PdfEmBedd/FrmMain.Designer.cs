﻿namespace PdfEmBed
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpInputFile = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.tbInputFile = new System.Windows.Forms.TextBox();
            this.lblInputFile = new System.Windows.Forms.Label();
            this.gbAttachments = new System.Windows.Forms.GroupBox();
            this.lbAttachments = new System.Windows.Forms.ListBox();
            this.flpAttachmentsControls = new System.Windows.Forms.FlowLayoutPanel();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.ofdInputfile = new System.Windows.Forms.OpenFileDialog();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnEmbedd = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.ofdAttachment = new System.Windows.Forms.OpenFileDialog();
            this.gpInputFile.SuspendLayout();
            this.gbAttachments.SuspendLayout();
            this.flpAttachmentsControls.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpInputFile
            // 
            this.gpInputFile.Controls.Add(this.btnBrowse);
            this.gpInputFile.Controls.Add(this.tbInputFile);
            this.gpInputFile.Controls.Add(this.lblInputFile);
            this.gpInputFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.gpInputFile.Location = new System.Drawing.Point(0, 0);
            this.gpInputFile.Name = "gpInputFile";
            this.gpInputFile.Size = new System.Drawing.Size(531, 51);
            this.gpInputFile.TabIndex = 0;
            this.gpInputFile.TabStop = false;
            this.gpInputFile.Text = "Input file";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(446, 17);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // tbInputFile
            // 
            this.tbInputFile.Location = new System.Drawing.Point(68, 19);
            this.tbInputFile.Name = "tbInputFile";
            this.tbInputFile.ReadOnly = true;
            this.tbInputFile.Size = new System.Drawing.Size(372, 20);
            this.tbInputFile.TabIndex = 1;
            // 
            // lblInputFile
            // 
            this.lblInputFile.AutoSize = true;
            this.lblInputFile.Location = new System.Drawing.Point(12, 22);
            this.lblInputFile.Name = "lblInputFile";
            this.lblInputFile.Size = new System.Drawing.Size(50, 13);
            this.lblInputFile.TabIndex = 0;
            this.lblInputFile.Text = "Input file:";
            // 
            // gbAttachments
            // 
            this.gbAttachments.Controls.Add(this.lbAttachments);
            this.gbAttachments.Controls.Add(this.flpAttachmentsControls);
            this.gbAttachments.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbAttachments.Location = new System.Drawing.Point(0, 51);
            this.gbAttachments.Name = "gbAttachments";
            this.gbAttachments.Size = new System.Drawing.Size(531, 185);
            this.gbAttachments.TabIndex = 1;
            this.gbAttachments.TabStop = false;
            this.gbAttachments.Text = "Attachments";
            // 
            // lbAttachments
            // 
            this.lbAttachments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbAttachments.FormattingEnabled = true;
            this.lbAttachments.Location = new System.Drawing.Point(3, 16);
            this.lbAttachments.Name = "lbAttachments";
            this.lbAttachments.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbAttachments.Size = new System.Drawing.Size(525, 137);
            this.lbAttachments.TabIndex = 0;
            // 
            // flpAttachmentsControls
            // 
            this.flpAttachmentsControls.Controls.Add(this.btnRemove);
            this.flpAttachmentsControls.Controls.Add(this.btnAdd);
            this.flpAttachmentsControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flpAttachmentsControls.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flpAttachmentsControls.Location = new System.Drawing.Point(3, 153);
            this.flpAttachmentsControls.Name = "flpAttachmentsControls";
            this.flpAttachmentsControls.Size = new System.Drawing.Size(525, 29);
            this.flpAttachmentsControls.TabIndex = 1;
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(447, 3);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(366, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // ofdInputfile
            // 
            this.ofdInputfile.DefaultExt = "pdf";
            this.ofdInputfile.Filter = "PDF files|*.pdf";
            this.ofdInputfile.Title = "Select input file";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnClose);
            this.flowLayoutPanel1.Controls.Add(this.btnEmbedd);
            this.flowLayoutPanel1.Controls.Add(this.btnAbout);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 236);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(531, 28);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(453, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnEmbedd
            // 
            this.btnEmbedd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnEmbedd.Location = new System.Drawing.Point(372, 3);
            this.btnEmbedd.Name = "btnEmbedd";
            this.btnEmbedd.Size = new System.Drawing.Size(75, 23);
            this.btnEmbedd.TabIndex = 1;
            this.btnEmbedd.Text = "Embed";
            this.btnEmbedd.UseVisualStyleBackColor = true;
            this.btnEmbedd.Click += new System.EventHandler(this.btnEmbedd_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.Location = new System.Drawing.Point(291, 3);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(75, 23);
            this.btnAbout.TabIndex = 0;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // ofdAttachment
            // 
            this.ofdAttachment.Filter = "Any files (*.*)|*.*";
            this.ofdAttachment.Multiselect = true;
            this.ofdAttachment.Title = "Select attachments to embed";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 264);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.gbAttachments);
            this.Controls.Add(this.gpInputFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PdfEmBed {0}";
            this.gpInputFile.ResumeLayout(false);
            this.gpInputFile.PerformLayout();
            this.gbAttachments.ResumeLayout(false);
            this.flpAttachmentsControls.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpInputFile;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox tbInputFile;
        private System.Windows.Forms.Label lblInputFile;
        private System.Windows.Forms.GroupBox gbAttachments;
        private System.Windows.Forms.ListBox lbAttachments;
        private System.Windows.Forms.FlowLayoutPanel flpAttachmentsControls;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.OpenFileDialog ofdInputfile;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnEmbedd;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.OpenFileDialog ofdAttachment;
    }
}

